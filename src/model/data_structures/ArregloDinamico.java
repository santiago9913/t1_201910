
package model.data_structures;

/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamico implements IArregloDinamico {
	/**
	 * Capacidad maxima del arreglo
	 */
	private int tamanoMax;
	/**
	 * Numero de elementos en el arreglo (de forma compacta desde la posicion 0)
	 */
	private int tamanoAct;
	/**
	 * Arreglo de elementos de tamaNo maximo
	 */
	private String elementos[ ];

	/**
	 * Construir un arreglo con la capacidad maxima inicial.
	 * @param max Capacidad maxima inicial
	 */
	public ArregloDinamico( int max )
	{
		elementos = new String[max];
		tamanoMax = max;
		tamanoAct = 0;
	}

	public void agregar( String dato )
	{
		if ( tamanoAct == tamanoMax )
		{  // caso de arreglo lleno (aumentar tamaNo)
			tamanoMax = 2 * tamanoMax;
			String [ ] copia = elementos;
			elementos = new String[tamanoMax];
			for ( int i = 0; i < tamanoAct; i++)
			{
				elementos[i] = copia[i];
			} 
			System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
		}	
		elementos[tamanoAct] = dato;
		tamanoAct++;
	}

	public int darTamano() {
		// TODO implementar
		return tamanoAct;
	}

	public String darElemento(int i){
		// TODO implementar

		String elem = null; 

		if(elementos.length != 0) {
			if(elementos[i] != null) {
				elem = elementos[i];
			}
		}

		return elem; 
	}

	public String buscar(String dato) {
		// TODO implementar
		// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.

		String buscado = null; 
		if(elementos.length != 0) {
			for(String elemento : elementos) {
				if(elemento.compareTo(dato) == 0 && elemento != null) {
					buscado = elemento;
				}
			}
		}

		return buscado; 
	}

	public String eliminar(String dato) {
		// TODO implementar
		// Recomendacion: Usar el criterio de comparacion natural (metodo compareTo()) definido en Strings.

		String eliminado = null; 

		for(int i = 0; i < tamanoAct-1; i++) {
			if(elementos[i] == dato) {
				eliminado = elementos[i]; 
				elementos[i] = null; 
				String temp = elementos[tamanoAct-1]; 
				elementos[tamanoAct-1] = elementos[i]; 
				elementos[i] = temp; 
			}
			else if(i == tamanoAct-2) {
				if(elementos[i+1] == dato) {
					eliminado = elementos[i+1]; 
					elementos[i+1] = null; 
				}
			}
		}
		tamanoAct--; 
		return eliminado; 
	}
}
